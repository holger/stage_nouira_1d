#include "matrix.h"

void upwind(std::vector<double>& ux, const std::vector<double>& u, double dx)
{
  for(int i=1;i<u.size()-1;i++)
    ux[i]=(u[i]-u[i-1])/dx;
}

void downwind(std::vector<double>& ux, const std::vector<double>& u, double dx)
{
  for(int i=1;i<u.size()-1;i++)
    ux[i]=(u[i+1]-u[i])/dx;
}

void center(std::vector<double>& ux, const std::vector<double>& u, double dx)
{
  for(int i=1;i<u.size()-1;i++)
    ux[i]=(u[i+1]-u[i-1])/(2*dx);
}
