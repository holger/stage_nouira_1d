for i in {10..990..10}
do
    echo $i
    gnuplot -e "set terminal jpeg; set yra [0:1]; plot '../u-$i.txt' lw 3 w l" > u-$i.jpeg
done

prefix="u-"
for a in u-*.jpeg
do
    echo $a

    step=${a#$prefix}
#    step=${foo%$suffix}
    echo "${step}"

    b=$(printf %04d.jpeg ${step%.jpeg})
    if [ $a != $b ]; then
	mv $a $b
    fi
done

mencoder mf://*.jpeg -mf w=800:h=600:fps=25:type=jpg -ovc lavc -lavcopts vcodec=mpeg4:mbd=2:trell -oac copy -o output.avi
#ffmpeg -i pic%04d.jpeg movie.mpeg
