#include <assert.h>
#include <cmath>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <vector>

#include "derive.hpp"

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

double sqr(double a)
{
  return a*a;
}

double minmod(double u1, double u2)
{
  double u = 0;
  if (u1*u2 > 0) 
    if(fabs(u1) < fabs(u2)) u = u1;
    else 
      if (fabs(u2) <= fabs(u1)) u = u2;
  
  return u;
}

void periodic(std::vector<double>& vec)
{
  vec[0] = vec[vec.size()-4];
  vec[1] = vec[vec.size()-3];
  vec[vec.size()-2] = vec[3];
  vec[vec.size()-1] = vec[4];
}

int main(int argc, char* argv[])
{
  system("mkdir fields");

  cerr << "let us solve the advection equation ... \n";
  if(argc < 4) {
    cerr << "parameters : nx cfl nSteps\n";
    exit(1);
  }
  
  int nSteps = atoi(argv[3]);
  int nx = atoi(argv[1]);
  double cfl = atof(argv[2]);
  
  double lx = 1;
  double dx = lx/(nx-1);
  double v = 1;
  double dt = cfl*dx/fabs(v);
  
  std::vector<double> u(nx);
  std::vector<double> u_upwind(nx);
  std::vector<double> u_downwind(nx);
  std::vector<double> u_center(nx);
  std::vector<double> u_lf(nx);
  std::vector<double> u_lf0(nx);
  std::vector<double> u_lw(nx);
  std::vector<double> u_lw0(nx);
  std::vector<double> u_fromm(nx);
  std::vector<double> fi_fromm(nx);
  std::vector<double> fo_fromm(nx);
  std::vector<double> u_leer(nx);
  std::vector<double> si_leer(nx);
  std::vector<double> so_leer(nx);
  std::vector<double> fi_leer(nx);
  std::vector<double> fo_leer(nx);
  std::vector<double> u_kt(nx);
  std::vector<double> u_ktsd(nx);
  std::vector<double> u_ktsdrk2(nx);

  std::vector<double> ux(nx);
  std::vector<double> ux_upwind(nx);
  std::vector<double> ux_downwind(nx);
  std::vector<double> ux_center(nx);

  cerr << "allocated " << nx << " element vectors \n";

   for(int i=0;i<nx;i++)
     u[i]=sin(2*M_PI/(lx-4*dx)*i*dx);
  
  // for(int i=0;i<nx;i++)
  //   if(i*dx > 0.1 && i*dx < 0.3)
  //     u[i] = 1;
  //   else
  //     u[i] = 0;
    

  // for(int i=nx/2;i<nx;i++)
  //   u[i] = 0;
  // for(int i=0;i<nx/2;i++)
  //   u[i] = 1;

  // for(int i=0;i<nx;i++)
  //   u[i] = 0;
  // for(int i=150;i<200;i++)
  //   u[i] = 1;

  u_upwind = u;
  u_downwind = u;
  u_center = u;
  u_lw = u;
  u_lf = u;
  u_fromm = u;
  u_leer = u;
  u_kt = u;
  u_ktsd = u;
  u_ktsdrk2 = u;

  for(int i=1;i<nx-1;i++)
    ux[i]=2*M_PI/lx*cos(2*M_PI/lx*(i-1)*dx);
  
  downwind(ux_downwind,u,dx);
  center(ux_center,u,dx);

  periodic(ux_upwind);
  periodic(ux_downwind);
  periodic(ux_center);
  
  {
    ofstream out("u_init.txt");
    for(int i=0;i<nx;i++)
      out << i*dx << "\t" << u[i] 
	  << "\t" << ux[i] 
	  << "\t" << ux_upwind[i] 
	  << "\t" << ux_downwind[i] 
	  << "\t" << ux_center[i] <<  std::endl;
  }

  std::stringstream filenameStream;
  filenameStream << "fields/description.txt";
  std::ofstream out(filenameStream.str().c_str());
  
  out << "1\t x\n"
      << "2\t upwind\n"
      << "3\t downwind\n"
      << "4\t denter\n"
      << "5\t lax-friedrich\n"
      << "6\t lax-wendroff\n"
      << "7\t fromm\n"
      << "8\t van leer\n"
      << "9\t in-slope van leer\n"
      << "10\t out-slope van leer\n"
      << "11\t kurganov-tadmor fully discrete\n"
      << "12\t kurganov-tadmor semi discrete euler\n"
      << "13\t kurganov-tadmor semi discrete rk2\n";
  out.close();

  for(int step=0;step<=nSteps;step++) {
    std::cout << "step = " << step << std::endl;
    if(step % 1 == 0) {
      std::stringstream filenameStream;
      filenameStream << "fields/u-" << step << ".txt";
      std::ofstream out(filenameStream.str().c_str());
      for(int i=0;i<nx;i++)
	out << i*dx << "\t" << u_upwind[i] 
	    << "\t" << u_downwind[i] 
	    << "\t" << u_center[i]
	    << "\t" << u_lf[i] 
	    << "\t" << u_lw[i] 
	    << "\t" << u_fromm[i]
	    << "\t" << u_leer[i] 
	    << "\t" << si_leer[i] 
	    << "\t" << so_leer[i] 
	    << "\t" << u_kt[i] 
	    << "\t" << u_ktsd[i] 
	    << "\t" << u_ktsdrk2[i] << std::endl;
    }
    
    for(int i=1;i<nx-1;i++) {
      if(v > 0)
	ux_upwind[i] = (u_upwind[i]-u_upwind[i-1])/dx;
      else
	ux_upwind[i] = (u_upwind[i+1]-u_upwind[i])/dx;
    }

    for(int i=1;i<nx-1;i++)
      u_upwind[i] -= v*dt*ux_upwind[i];
    
    periodic(u_upwind);
    upwind(ux_upwind,u_upwind,dx);

    for(int i=1;i<nx-1;i++) 
      u_downwind[i] -= v*dt*ux_downwind[i];
    periodic(u_downwind);
    downwind(ux_downwind,u_downwind,dx);
    
    for(int i=1;i<nx-1;i++) 
      u_center[i] -= v*dt*ux_center[i];
    periodic(u_center);
    center(ux_center,u_center,dx);

    // lax-friedrich ----------------------------------------------------------------------------------------

    u_lf0 = u_lf;
    for(int i=1;i<nx-1;i++) 
      u_lf[i] = 0.5*(u_lf0[i+1]+u_lf0[i-1]) - 0.5*v*dt/dx*(u_lf0[i+1]-u_lf0[i-1]);
    periodic(u_lf);

    // lax-wendroff -----------------------------------------------------------------------------------------

    u_lw0 = u_lw;
    for(int i=1;i<nx-1;i++) 
      u_lw[i] = (1-sqr(v*dt/dx))*u_lw0[i] - 0.5*v*dt/dx*((1-v*dt/dx)*u_lw0[i+1]-(1+v*dt/dx)*u_lw0[i-1]);
    periodic(u_lw);
    
    // fromm -------------------------------------------------------------------------------------------------
    
    if(v > 0) {
      for(int i=2;i<nx;i++)
	fi_fromm[i] = v*(u_fromm[i-1] + 0.5*(1-cfl)*(u_fromm[i] - u_fromm[i-2])/2);
      for(int i=1;i<nx-1;i++)
	fo_fromm[i] = v*(u_fromm[i] + 0.5*(1-cfl)*(u_fromm[i+1] - u_fromm[i-1])/2);
    }
    else {
      for(int i=0;i<nx-2;i++)
	fi_fromm[i] = -v*(u_fromm[i+1] - 0.5*(1-cfl)*(u_fromm[i+2] - u_fromm[i])/2);
      for(int i=1;i<nx-1;i++)
	fo_fromm[i] = -v*(u_fromm[i] - 0.5*(1-cfl)*(u_fromm[i+1] - u_fromm[i-1])/2);
    }

    for(int i=2;i<nx-2;i++)
      u_fromm[i] += dt/dx*(fi_fromm[i] - fo_fromm[i]);

    periodic(u_fromm);
    
    // van-leer ----------------------------------------------------------------------------------------------

    if(v > 0) {
      for(int i=2;i<nx;i++) {
	double phi = sgn((u_leer[i]-u_leer[i-1])*(u_leer[i-1]-u_leer[i-2]));
	double slope = 0;
	si_leer[i] = 0;
	if(phi == 1) {
	  slope = 2*fabs(u_leer[i]-u_leer[i-1]);
	  si_leer[i] = 1;
	  double slope1 = 2*fabs(u_leer[i-1]-u_leer[i-2]);
	  double slope2 = 0.5*fabs(u_leer[i]-u_leer[i-2]);
	  if(slope1 < slope) { slope = slope1; si_leer[i] = 2; }
	  if(slope2 < slope) { slope = slope2; si_leer[i] = 3; }
	  slope *= sgn(u_leer[i]-u_leer[i-2]);
	}
	fi_leer[i] = v*(u_leer[i-1] + 0.5*(1-cfl)*slope);
      }

      for(int i=1;i<nx-1;i++) {
	double phi = sgn((u_leer[i+1]-u_leer[i])*(u_leer[i]-u_leer[i-1]));
	double slope = 0;
	so_leer[i] = 0;
	if(phi == 1) {
	  slope = 2*fabs(u_leer[i+1]-u_leer[i]);
	  so_leer[i] = 1;
	  double slope1 = 2*fabs(u_leer[i]-u_leer[i-1]);
	  double slope2 = 0.5*fabs(u_leer[i+1]-u_leer[i-1]);
	  if(slope1 < slope) { slope = slope1; so_leer[i] = 2; }
	  if(slope2 < slope) { slope = slope2; so_leer[i] = 3; }
	  slope *= sgn(u_leer[i+1]-u_leer[i-1]);
	}
	fo_leer[i] = v*(u_leer[i] + 0.5*(1-cfl)*slope);
      }
    }
    else {
      for(int i=2;i<nx;i++) {
	double phi = sgn((u_leer[i+2]-u_leer[i+1])*(u_leer[i+1]-u_leer[i]));
	double slope = 0;
	si_leer[i] = 0;
	if(phi == 1) {
	  slope = 2*fabs(u_leer[i+1]-u_leer[i]);
	  si_leer[i] = 1;
	  double slope1 = 2*fabs(u_leer[i+2]-u_leer[i+1]);
	  double slope2 = 0.5*fabs(u_leer[i+2]-u_leer[i]);
	  if(slope1 < slope) { slope = slope1; si_leer[i] = 2; }
	  if(slope2 < slope) { slope = slope2; si_leer[i] = 3; }
	  slope *= sgn(u_leer[i+2]-u_leer[i]);
	}
	fi_leer[i] = -v*(u_leer[i+1] - 0.5*(1-cfl)*slope);
      }
      
      for(int i=1;i<nx-1;i++) {
	double phi = sgn((u_leer[i+1]-u_leer[i])*(u_leer[i]-u_leer[i-1]));
	double slope = 0;
	so_leer[i] = 0;
	if(phi == 1) {
	  slope = 2*fabs(u_leer[i+1]-u_leer[i]);
	  so_leer[i] = 1;
	  double slope1 = 2*fabs(u_leer[i]-u_leer[i-1]);
	  double slope2 = 0.5*fabs(u_leer[i+1]-u_leer[i-1]);
	  if(slope1 < slope) { slope = slope1; so_leer[i] = 2; }
	  if(slope2 < slope) { slope = slope2; so_leer[i] = 3; }
	  slope *= sgn(u_leer[i+1]-u_leer[i-1]);
	}
	fo_leer[i] = -v*(u_leer[i] - 0.5*(1-cfl)*slope);
      }
    }

    for(int i=2;i<nx-1;i++)
      u_leer[i] += dt/dx*(fi_leer[i] - fo_leer[i]);

    periodic(u_leer);

    // Kurganov-Tadmor - fully discrete
    
    std::vector<double> uxnj(nx);
    std::vector<double> onp1jp5(nx);
    std::vector<double> onp1j(nx);
    std::vector<double> uxnp1jp5(nx);
    
    double lambda = dt/dx;
    double anjp5 = fabs(v);
    double anjm5 = fabs(v);
    double anjp3 = fabs(v);
    
    for(int i=1;i<nx-1;i++) 
      uxnj[i] = minmod((u_kt[i] - u_kt[i-1])/dx,(u_kt[i+1] - u_kt[i])/dx);
    
    for(int i=1;i<nx-1;i++) {
      double unjp5r = u_kt[i+1] - dx*uxnj[i+1]*(1-lambda*anjp5);
      double unjp5l = u_kt[i] + dx*uxnj[i+1]*(1-lambda*anjp5);
      double unjm5r = u_kt[i] - dx*uxnj[i]*(1-lambda*anjm5);
      
      double unp5jp5r = unjp5r - 0.5*dt*v*uxnj[i+1];
      double unp5jp5l = unjp5l - 0.5*dt*v*uxnj[i];
      double unp5jm5r = unjm5r - 0.5*dt*v*uxnj[i];
      
      onp1jp5[i] = 0.5*(u_kt[i] + u_kt[i+1]) + 0.25*(dx - anjp5*dt)*(uxnj[i] - uxnj[i+1]) - 0.5/anjp5*(v*unp5jp5r - v*unp5jp5l);
      onp1j[i] = u_kt[i] + 0.5*dt*(anjm5-anjp5)*uxnj[i] - lambda/(1-lambda*(anjm5 + anjp5))*(v*unp5jp5l - v*unp5jm5r);
    }
    
    for(int i=1;i<nx-1;i++) {
      uxnp1jp5[i] = 2./dx*minmod((onp1j[i+1] - onp1jp5[i])/(1+lambda*(anjp5 - anjp3)), (onp1jp5[i] - onp1j[i])/(1+lambda*(anjp5 - anjm5)));
    }
    
    for(int i=1;i<nx-1;i++) {
      u_kt[i] = lambda*anjm5*onp1jp5[i-1] + lambda*anjp5*onp1jp5[i] + (1 - lambda*(anjm5+anjp5))*onp1j[i] + 0.5*dx*(sqr(lambda*anjm5)*uxnp1jp5[i-1] - sqr(lambda*anjp5)*uxnp1jp5[i]);
    }
    
    periodic(u_kt);

    // Kurganov-Tadmor - semi discrete

    std::vector<double> u_ktsd0(u_ktsd);

    for(int i=1;i<nx-1;i++) 
      uxnj[i] = minmod((u_ktsd[i] - u_ktsd[i-1])/dx,(u_ktsd[i+1] - u_ktsd[i])/dx);

    // for(int i=1;i<nx-1;i++) 
    //   uxnj[i] = (u_ktsd[i+1] - u_ktsd[i])/(dx);

    for(int i=1;i<nx-1;i++) {
      double upjp5 = u_ktsd[i+1] - 0.5*dx*uxnj[i+1];
      double umjp5 = u_ktsd[i] + 0.5*dx*uxnj[i];
      double upjm5 = u_ktsd[i] - 0.5*dx*uxnj[i];
      double umjm5 = u_ktsd[i-1] + 0.5*dx*uxnj[i-1];

      double ajp5 = fabs(v);
      double ajm5 = fabs(v);

      u_ktsd0[i] = u_ktsd[i] + dt*(-0.5/dx*((v*upjp5 + v*umjp5) - (v*upjm5 + v*umjm5)) + 0.5/dx*(ajp5*(upjp5 - umjp5) - ajm5*(upjm5 - umjm5)));
    }
    u_ktsd = u_ktsd0;
    periodic(u_ktsd);

    // Kurganov-Tadmor - semi discrete - rk2

    for(int i=1;i<nx-1;i++) 
      uxnj[i] = minmod((u_ktsdrk2[i] - u_ktsdrk2[i-1])/dx,(u_ktsdrk2[i+1] - u_ktsdrk2[i])/dx);

    for(int i=1;i<nx-1;i++) {
      double upjp5 = u_ktsdrk2[i+1] - 0.5*dx*uxnj[i+1];
      double umjp5 = u_ktsdrk2[i] + 0.5*dx*uxnj[i];
      double upjm5 = u_ktsdrk2[i] - 0.5*dx*uxnj[i];
      double umjm5 = u_ktsdrk2[i-1] + 0.5*dx*uxnj[i-1];

      double ajp5 = fabs(v);
      double ajm5 = fabs(v);

      u_ktsd0[i] = u_ktsdrk2[i] + 0.5*dt*(-0.5/dx*((v*upjp5 + v*umjp5) - (v*upjm5 + v*umjm5)) + 0.5/dx*(ajp5*(upjp5 - umjp5) - ajm5*(upjm5 - umjm5)));
    }

    periodic(u_ktsd0);

    for(int i=1;i<nx-1;i++) 
      uxnj[i] = minmod((u_ktsd0[i] - u_ktsd0[i-1])/dx,(u_ktsd0[i+1] - u_ktsd0[i])/dx);

    for(int i=1;i<nx-1;i++) {
      double upjp5 = u_ktsd0[i+1] - 0.5*dx*uxnj[i+1];
      double umjp5 = u_ktsd0[i] + 0.5*dx*uxnj[i];
      double upjm5 = u_ktsd0[i] - 0.5*dx*uxnj[i];
      double umjm5 = u_ktsd0[i-1] + 0.5*dx*uxnj[i-1];

      double ajp5 = fabs(v);
      double ajm5 = fabs(v);

      u_ktsdrk2[i] = u_ktsdrk2[i] + dt*(-0.5/dx*((v*upjp5 + v*umjp5) - (v*upjm5 + v*umjm5)) + 0.5/dx*(ajp5*(upjp5 - umjp5) - ajm5*(upjm5 - umjm5)));
    }

    periodic(u_ktsdrk2);
  }
}
  
